//    Write a function that accesses and logs the name and city of the individual at the index position 3 in the dataset.

const arrayOfObjects = require("./studentsData");

function DataAt3(data, index) {
  if (
    Array.isArray(data) &&
    typeof index === "number" &&
    index <= data.length
  ) {
    return [data[index - 1].name, data[index - 1].city];
  } else {
    return [];
  }
}
console.log(DataAt3(arrayOfObjects, 3));
