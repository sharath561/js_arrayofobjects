//    Implement a loop to access and print the ages of all individuals in the dataset.

const arrayOfObjects = require("./studentsData");
function allAges(data) {
  if (Array.isArray(data)) {
    const agesData = [];

    for (let student = 0; student < data.length; student++) {
      agesData.push(data[student].age);
    }

    return agesData;
  } else {
    return [];
  }
}

console.log(allAges(arrayOfObjects));
