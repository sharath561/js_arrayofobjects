//  Create a function that extracts and displays the names of individuals who are students (`isStudent: true`) and live in Australia.
const arrayOfObjects = require("./studentsData");
function studentAustralia(data) {
  if (Array.isArray(data)) {
    const studentData = [];

    for (let student = 0; student < data.length; student++) {
      if (data[student].isStudent && data[student].country == "Australia") {
        studentData.push(data[student].name);
      }
    }

    return studentData;
  } else {
    return [];
  }
}

console.log(studentAustralia(arrayOfObjects));
