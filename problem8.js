//    Implement a loop to access and log the city and country of each individual in the dataset.
const arrayOfObjects = require("./studentsData");
function cityCountry(data) {

    if (Array.isArray(data)){

        const studentData = [];

        for (let student = 0; student < data.length; student++) {
            studentData.push([data[student].city, data[student].country]);
          }

        return studentData


    }
    else{
        return []
    }
  
}

console.log(cityCountry(arrayOfObjects));
