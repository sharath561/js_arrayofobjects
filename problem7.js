// Write a function that accesses and prints the names and email addresses of individuals aged 25.

const arrayOfObjects = require("./studentsData");
function namesEmail25(data) {
  if (Array.isArray(data)) {
    const studentData = [];

    for (let student = 0; student < data.length; student++) {
      if (data[student].age == 25) {
        studentData.push([data[student].name, data[student].email]);
      }
    }

    return studentData;
  } else {
    return [];
  }
}

console.log(namesEmail25(arrayOfObjects));
