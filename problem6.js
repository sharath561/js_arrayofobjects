//    Create a function to retrieve and display the first hobby of each individual in the dataset.

const arrayOfObjects = require("./studentsData");

function firtsHobby(data) {
  if (Array.isArray(data)) {
    const hobbiesData = [];

    for (let student = 0; student < data.length; student++) {
      hobbiesData.push(data[student].hobbies[0]);
    }

    return hobbiesData;
  } else {
    return [];
  }
}

console.log(firtsHobby(arrayOfObjects));
