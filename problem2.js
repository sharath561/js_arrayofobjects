//    Implement a function that retrieves and prints the hobbies of individuals with a specific age, say 30 years old.

const arrayOfObjects = require("./studentsData");
function hobbies(data) {
  if (Array.isArray(data)) {
    const hobbiesData = [];

    for (let student = 0; student < data.length; student++) {
      if (data[student].age == 30) {
        hobbiesData.push(data[student].hobbies);
      }
    }

    return hobbiesData;
  } else {
    return [];
  }
}

console.log(hobbies(arrayOfObjects));
