//    Given the dataset of individuals, write a function that accesses and returns the email addresses of all individuals.
const arrayOfObjects = require("./studentsData");

function emailAddress(data) {
  if (Array.isArray(data)) {
    const emailsData = [];

    for (let student = 0; student < data.length; student++) {
      emailsData.push(data[student].email);
    }

    return emailsData;
  }
  else{
    return [];
  }
}

console.log(emailAddress(arrayOfObjects));
